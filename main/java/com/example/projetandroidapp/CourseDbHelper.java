package com.example.projetandroidapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.projetandroidapp.Course;

import java.util.ArrayList;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class CourseDbHelper extends SQLiteOpenHelper {

    private static final String TAG = CourseDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "edtceri.db";

    public static final String TABLE_NAME = "course";

    public static final String _ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_DTSTART = "dtstart";
    public static final String COLUMN_DTEND = "dtend";

    public static final String[] COLUMNS = {_ID,COLUMN_TITLE,COLUMN_DESCRIPTION,COLUMN_TYPE,COLUMN_DTSTART,COLUMN_DTEND};

    public CourseDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +

                COLUMN_TITLE + " TEXT NOT NULL, " +
                COLUMN_DESCRIPTION + " TEXT NOT NULL, " +
                COLUMN_TYPE + " INTEGER, " +
                COLUMN_DTSTART+ " TEXT, " +
                COLUMN_DTEND+ " TEXT" +");";

        db.execSQL(SQL_CREATE_BOOK_TABLE);

        populate(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    /**
     * Adds a new course
     * @return  true if the course was added to the table ; false otherwise (case when the pair (course name, country) is
     * already in the data base
     */
    public boolean addCourse(Course course) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, course.getTitle());
        values.put(COLUMN_DESCRIPTION, course.getDescription());
        values.put(COLUMN_TYPE, course.getType());
        values.put(COLUMN_DTSTART, course.getDateStartToString());
        values.put(COLUMN_DTEND, course.getDateEndToString());

        Log.d(TAG, "adding: "+course.getTitle()+" with id="+course.getId());

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (course name, country)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    public boolean addCourse(SQLiteDatabase db,Course course) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, course.getTitle());
        values.put(COLUMN_DESCRIPTION, course.getDescription());
        values.put(COLUMN_TYPE, course.getType());
        values.put(COLUMN_DTSTART, course.getDateStartToString());
        values.put(COLUMN_DTEND, course.getDateEndToString());

        Log.d(TAG, "adding: "+course.getTitle()+" with id="+course.getId());

        // Inserting Row
        // The unique used for creating table ensures to have only one copy of each pair (course name, country)
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        return (rowID != -1);
    }

    /**
     * Updates the information of a course inside the data base
     * @return the number of updated rows
     */
    public int updateCourse(Course course) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, course.getTitle());
        values.put(COLUMN_DESCRIPTION, course.getDescription());
        values.put(COLUMN_TYPE, course.getType());
        values.put(COLUMN_DTSTART, course.getDateStartToString());
        values.put(COLUMN_DTEND, course.getDateEndToString());

        // updating row
        return db.updateWithOnConflict(TABLE_NAME, values, _ID + " = ?",
                new String[] { String.valueOf(course.getId()) }, CONFLICT_IGNORE);
    }

    /**
     * Returns a cursor on all the cities of the data base
     */
    public Cursor fetchAllCourses() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, COLUMNS,
                null, null, null, null, "datetime("+COLUMN_DTSTART+") ASC", null);

        Log.d(TAG, "call fetchAllCities()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public Cursor fetchCourseByDate(String strDate) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, COLUMNS,
                "date("+COLUMN_DTSTART+") = date('"+strDate+"') ", null, null, null, "datetime("+COLUMN_DTSTART+") ASC", null);

        Log.d(TAG, "call fetchAllCities()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public Cursor getHomeCourse(String strDate) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, COLUMNS,
                "date("+COLUMN_DTSTART+") = date('"+strDate+"') ", null, null, null, "datetime("+COLUMN_DTSTART+") ASC", "2");

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public String getNextEvaluation(String strDate) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, COLUMNS,
                "date("+COLUMN_DTSTART+") >= date('"+strDate+"') and "+COLUMN_DESCRIPTION+" LIKE '%Evaluation%'", null, null, null, "datetime("+COLUMN_DTSTART+") ASC", null);

        String strDate2="";
        if (cursor != null) {
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                strDate2 = cursor.getString(cursor.getColumnIndex(COLUMN_DTSTART));
                strDate2 = strDate2.substring(0,10);
            }
        }else{
            strDate2="";
        }

        return strDate2;
    }

    public String getPreviewEvaluation(String strDate) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, COLUMNS,
                "date("+COLUMN_DTSTART+") <= date('"+strDate+"') and "+COLUMN_DESCRIPTION+" LIKE '%Evaluation%'", null, null, null, "datetime("+COLUMN_DTSTART+") DESC", null);

        String strDate2="";
        if (cursor != null) {
            cursor.moveToFirst();
            if(cursor.getCount()>0){
                strDate2 = cursor.getString(cursor.getColumnIndex(COLUMN_DTSTART));
                strDate2 = strDate2.substring(0,10);
            }
        }else{
            strDate2="";
        }

        return strDate2;
    }

    public Cursor fetchEvaluationByDate(String strDate) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, COLUMNS,
                "date("+COLUMN_DTSTART+") = date('"+strDate+"') and "+COLUMN_DESCRIPTION+" LIKE '%Evaluation%'", null, null, null, "datetime("+COLUMN_DTSTART+") ASC", null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    /**
     * Returns a list on all the cities of the data base
     */
    public ArrayList<Course> getAllCities() {
        ArrayList<Course> res = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, COLUMNS,
                null, null, null, null, "datetime("+COLUMN_DTSTART+") ASC", null);

        if (cursor != null) {
            while(cursor.moveToNext()){
                Course course = cursorToCourse(cursor);
                res.add(course);
            }
        }

        return res;
    }

    public ArrayList<Course> getCourseByDate(String strDate) {
        ArrayList<Course> res = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, COLUMNS,
                COLUMN_DTSTART+" LIKE '%"+strDate+"%'", null, null, null, "datetime("+COLUMN_DTSTART+") ASC", null);

        if (cursor != null) {
            while(cursor.moveToNext()){
                Course course = cursorToCourse(cursor);
                res.add(course);
            }
        }

        return res;
    }

    public void deleteCourse(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID + " = ?",
                new String[]{cursor.getString(cursor.getColumnIndex(_ID))});
        db.close();
    }

    public void deleteCourse(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    public void deleteAllCourses() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME,"",null);
        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        /*addCourse(new Course("Avignon","France"));
        addCourse(new Course("Paris","France"));
        addCourse(new Course("Rennes","France"));
        addCourse(new Course("Montreal","Canada"));
        addCourse(new Course("Fortaleza","Brazil"));
        addCourse(new Course("Papeete","French Polynesia"));
        addCourse(new Course("Sydney","Australia"));
        addCourse(new Course("Seoul","South Korea"));
        addCourse(new Course("Bamako","Mali"));*/

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    public void populate(SQLiteDatabase db) {
        Log.d(TAG, "call populate()");
        /*addCourse(db,new Course("Avignon","France"));
        addCourse(db,new Course("Paris","France"));
        addCourse(db,new Course("Rennes","France"));
        addCourse(db,new Course("Montreal","Canada"));
        addCourse(db,new Course("Fortaleza","Brazil"));
        addCourse(db,new Course("Papeete","French Polynesia"));
        addCourse(db,new Course("Sydney","Australia"));
        addCourse(db,new Course("Seoul","South Korea"));
        addCourse(db,new Course("Bamako","Mali"));*/
    }

    public static Course cursorToCourse(Cursor cursor) {
        Course course = new Course(cursor.getLong(cursor.getColumnIndex(_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
                cursor.getString(cursor.getColumnIndex(COLUMN_TYPE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DTSTART)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DTEND))
        );
        return course;
    }

    public Course getCourse(int id) {
        Course course=null;
        // TODO
        return course;
    }
}