package com.example.projetandroidapp;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class EmploiDuJour extends AppCompatActivity {
   // public static final String TAG = City.class.getSimpleName();
    CourseDbHelper db;
    private Calendar cal = Calendar.getInstance();
    private int eventIndex;
    private RelativeLayout mLayout;
    private TextView currentDate;
    ImageView previousDay;
    ImageView nextDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emploi_du_jour);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        currentDate = findViewById(R.id.display_current_date);
        currentDate.setText(displayDateInString(cal.getTime()));

        mLayout = findViewById(R.id.left_event_column);
        eventIndex = mLayout.getChildCount();
        db=new CourseDbHelper(this);
        displayDailyEvents();

        previousDay = findViewById(R.id.previous_day);
        nextDay = findViewById(R.id.next_day);
        previousDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previousCalendarDate();
            }
        });
        nextDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextCalendarDate();
            }
        });

    }

    private void previousCalendarDate(){
        mLayout.removeViews(eventIndex-1,mLayout.getChildCount()-eventIndex);
        //Log.i("db_s", " views deleted = " + (mLayout.getChildCount()-eventIndex+1) ) ;
        cal.add(Calendar.DAY_OF_MONTH, -1);
        currentDate.setText(displayDateInString(cal.getTime()));
        displayDailyEvents();
    }
    private void nextCalendarDate(){
        mLayout.removeViews(eventIndex-1,mLayout.getChildCount()-eventIndex);
        //Log.i("db_s", " views deleted = " + (mLayout.getChildCount()-eventIndex+1) ) ;
        cal.add(Calendar.DAY_OF_MONTH, 1);
        currentDate.setText(displayDateInString(cal.getTime()));
        displayDailyEvents();
    }

    private String displayDateInString(Date mDate){
        SimpleDateFormat formatter = new SimpleDateFormat("d MMMM, yyyy", Locale.FRENCH);
        return formatter.format(mDate);
    }

    private String extractHourInString(Date mDate){
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm", Locale.FRENCH);
        return formatter.format(mDate);
    }

    private void displayDailyEvents(){
        Cursor cursor=db.fetchCourseByDate(new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
        //Cursor cursor=db.fetchCourseByDate("2019-04-04");
        cursor.moveToPosition(-1);
        while(cursor.moveToNext()){
            Course course = db.cursorToCourse(cursor);
            Date dateStart = course.getDateStart();
            Date dateEnd = course.getDateEnd();
            String description = course.getDescription();
            Log.i("db_",course.getDateStart()+"-"+course.getDateEndToString()+" "+course.getTitle()+" "+course.getType());
            int eventBlockHeight = getEventTimeFrame(dateStart, dateEnd);
            Log.i("db_", "Height " + eventBlockHeight);
            String delay = extractHourInString(dateStart)+"-"+extractHourInString(dateEnd);
            String type = course.getType();
            String title = delay+"/"+type;
            displayEventSection(dateStart, eventBlockHeight, title, description);
        }
        //Log.i("db_c", " childs = " + eventIndex + " childs_new = " + mLayout.getChildCount() ) ;
        //Log.i("db_c", " add = " + (mLayout.getChildCount() - eventIndex ) ) ;
    }
    private int getEventTimeFrame(Date start, Date end){
        long timeDifference = end.getTime() - start.getTime();
        Calendar mCal = Calendar.getInstance();
        mCal.setTimeInMillis(timeDifference);
        int hours = mCal.get(Calendar.HOUR);
        int minutes = mCal.get(Calendar.MINUTE);
        long minutes_ = TimeUnit.MILLISECONDS.toMinutes(timeDifference);
        //return ((hours * 60) + ((minutes * 60) / 100))/2;
        //Log.i("db_", "Minutes  " + minutes_);
        return (int) minutes_;
    }
    private void displayEventSection(Date eventDate, int height, String title, String description){
        SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm", Locale.FRENCH);
        String displayValue = timeFormatter.format(eventDate);
        Log.i("db_", "Heure "+eventDate+" - format " + displayValue);
        String[]hourMinutes = displayValue.split(":");
        int hours = Integer.parseInt(hourMinutes[0]);
        int minutes = Integer.parseInt(hourMinutes[1]);
        int topViewMargin = hours*60 + (minutes-480);
        Log.i("db_", "Margin top " + topViewMargin);

        createEventView(topViewMargin, height, title, description);
    }
    private void createEventView(int topMargin, int height, String title, String description){
        RelativeLayout.LayoutParams lParam = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lParam.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        lParam.topMargin = topMargin * 2;
        lParam.leftMargin = 0;

        TextView mEventView = new TextView(getApplication());
        Cours cours = new Cours(getApplication());
        cours.setLayoutParams(lParam);
        cours.setPadding(10, 0, 10, 0);
        cours.relativeLayout.setGravity(0x11);
        cours.setLayoutHeigh(height * 2);
        mEventView.setTextColor(Color.parseColor("#ffffff"));

        if(description.contains("Evaluation"))
            cours.title.setBackgroundColor(Color.RED);

        cours.title.setText(title);
        cours.description.setText(String.format(description.replace("\\n", "%n")));
        mLayout.addView(cours,mLayout.getChildCount()-1);
    }
}
