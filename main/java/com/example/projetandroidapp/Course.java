package com.example.projetandroidapp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Course {
    public static final String TAG = Course.class.getSimpleName();
    private long id;
    private String title;
    private String teacher;
    private String group;
    private String classroom;
    private String type;
    private Date dateStart;
    private Date dateEnd;
    private String description;

    public Course() {
        this.title = "";
        this.group = "";
        this.classroom = "";
        this.type = "";
        this.dateStart = null;
        this.dateEnd = null;
        this.description = "";

    }

    public Course( long id,String title, String description,String type, String dateStart, String dateEnd) {
        this.id=id;
        this.title = title;
        this.description = description;
        this.teacher = "";
        this.group = "";
        this.classroom = "";
        this.type = type;
        this.setDateStart(dateStart);
        this.setDateEnd(dateEnd);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTeacher() {
        return teacher;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDateStart() {
        return dateStart;
    }
    public String getDateStartToString() { return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRANCE).format(this.dateStart);  }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }
    public void setDateStart(String strDateStart){
        try {
            this.dateStart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRANCE).parse(strDateStart);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Date getDateEnd() {
        return dateEnd;
    }
    public String getDateEndToString() { return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRANCE).format(this.dateEnd);  }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }
    public void setDateEnd(String strDateEnd) {
        try {
            this.dateEnd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRANCE).parse(strDateEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}