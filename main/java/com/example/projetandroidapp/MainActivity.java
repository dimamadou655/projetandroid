package com.example.projetandroidapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    LinearLayout   linearLayout;
    private BottomNavigationView navigation;
    private FrameLayout frameLayout;
    boolean firsTime=true;
    CourseDbHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //navigation = (BottomNavigationView) findViewById(R.id.nav_view);
       // navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
       // BottomNavigationViewHelper.disableShiftMode(navigation);

        linearLayout = findViewById(R.id.linearLayout);
         db= new CourseDbHelper(this);

        MyAsyncTask asyncTask = new MyAsyncTask() ;
        asyncTask.execute();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private class MyAsyncTask extends AsyncTask<String, String, String> {

        private String resp=null;
        ProgressDialog progressDialog;
        ICALResponseHandler icalResponse;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                db.deleteAllCourses();
                icalResponse.readIcalStream(db);
            } catch (Exception e) {
                resp = e.getMessage();
                e.printStackTrace();
            }
            return resp;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MainActivity.this,
                    "Mise à jour",
                    "Veuillez patienter un instant ...");
        }

        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
            firsTime=false;
            Calendar cal = Calendar.getInstance();
            Cursor cursor = db.getHomeCourse(new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
            cursor.moveToPosition(-1);
            while(cursor.moveToNext()){
                Course course = db.cursorToCourse(cursor);

                Cours cours = new Cours(getApplication());
                //cours.setLayoutParams(lParam);
                cours.setPadding(10, 0, 10, 0);
                cours.relativeLayout.setGravity(0x11);
                cours.setLayoutHeigh(200);

                if(course.getDescription().contains("Evaluation"))
                    cours.title.setBackgroundColor(Color.RED);

                cours.title.setText(course.getTitle());
                cours.description.setText(String.format(course.getDescription().replace("\\n", "%n")));
                Log.i("TAG_",course.getDescription());
                linearLayout.addView(cours);
            }
        }

        @Override
        protected void onProgressUpdate(String... text) {

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_emploi) {
            Intent intent = new Intent(MainActivity.this,EmploiDuJour.class);
          //  Cursor cursor = (Cursor) linearLayout.getItemAtPosition(position);
            //Course course =  CourseDbHelper.cursorToCourse(cursor);
            //intent.putExtra(Course.TAG, (Parcelable) course);
            startActivityForResult(intent,1);

        } else if (id == R.id.nav_controle) {
            Intent intent = new Intent(MainActivity.this,Controle.class);
            //intent.putExtra(City.TAG,city);
            startActivityForResult(intent,1);

        } else if (id == R.id.nav_salle) {
            Intent intent = new Intent(MainActivity.this,SalleLibre.class);
            //intent.putExtra(City.TAG,city);
            startActivityForResult(intent,1);

        } else if (id == R.id.nav_evolution) {
            Intent intent = new Intent(MainActivity.this,EvolutionDesUE.class);
            //intent.putExtra(City.TAG,city);
            startActivityForResult(intent,1);

        }else if (id == R.id.nav_choix) {
            Intent intent = new Intent(MainActivity.this,ChoixDuTD.class);
            //intent.putExtra(City.TAG,city);
            startActivityForResult(intent,1);

        }
        else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
