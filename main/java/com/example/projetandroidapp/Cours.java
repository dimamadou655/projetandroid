package com.example.projetandroidapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Cours extends CardView {

    public TextView title;
    public TextView description;
    public RelativeLayout relativeLayout;

    public Cours(@NonNull Context context) {
        super(context);
        init();
    }

    public Cours(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void init(){
        inflate(getContext(),R.layout.cours,this);
        this.title = findViewById(R.id.coursetitle);
        this.description = findViewById(R.id.coursedescription);
        this.relativeLayout = findViewById(R.id.relativeLayout);
    }

    public void setLayoutHeigh(int h){
        relativeLayout.getLayoutParams().height=h;
    }

}
