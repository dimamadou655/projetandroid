package com.example.projetandroidapp;


import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {

    private static final String HOST = "edt-api.univ-avignon.fr";
    private static final String PATH_1 = "app.php";
    private static final String PATH_2 = "api";
    private static final String PATH_3 = "exportAgenda";
    private static final String PATH_4 = "diplome";
    private static final String PATH_TD = "2-L3IN";

    private static final String HOST2 = "accueil-ent2.univ-avignon.fr";
    private static final String PATH2_1 = "edt";
    private static final String PATH2_2 = "exportAgendaUrl";
    private static final String KEY2_ACTION ="codeDip";
    private static final String PARAM_TD = "2-L3IN";

    public static URL getUrl() throws MalformedURLException {

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1)
                .appendPath(PATH_2)
                .appendPath(PATH_3)
                .appendPath(PATH_4)
                .appendPath(PATH_TD);
        URL url = new URL(builder.build().toString());
        /*https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN*/
        return url;
    }

    public static URL getOldUrl() throws MalformedURLException {

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority(HOST2)
                .appendPath(PATH2_1)
                .appendPath(PATH2_2)
                .appendQueryParameter(KEY2_ACTION, PARAM_TD);
        URL url = new URL(builder.build().toString());
        /*https://accueil-ent2.univ-avignon.fr/edt/exportAgendaUrl?codeDip=2-L3IN*/
        return url;
    }

}