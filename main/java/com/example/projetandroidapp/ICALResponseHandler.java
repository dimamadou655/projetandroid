package com.example.projetandroidapp;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Process the response to a GET request to the Web service
 * api.openweathermap.org
 * Responses must be provided in JSON.
 *
 * Log.i("","----------------------------------------------------------------------------------------------------");
 *
 */


public class ICALResponseHandler {

    private static final String TAG = ICALResponseHandler.class.getSimpleName();

    private Course media;
    private static String PROMOTION="L3 INFORMATIQUE";
    private static String TD="L3INFO_TD2";

    public ICALResponseHandler(Course media) {
        this.media = media;
    }

    /**
     *
     * @return A City with attributes filled with the collected information if response was
     * successfully analyzed; a void list otherwise
     */

    public static ArrayList<Course> readIcalStream(CourseDbHelper db) throws IOException {
        ArrayList<Course> theCourses=new ArrayList<>();
        theCourses.clear();

        URL link = WebServiceUrl.getUrl();
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(link.openStream(),"UTF-8"));
            int i =0;
            String inputLine;
            Course course = null;
            Boolean canceled=false;
            Date date =null;
            DateFormat dateFormat=new SimpleDateFormat("yyyyMMdd'T'HHmmss", Locale.FRANCE);
            String date_str ="";

            while ((inputLine = in.readLine()) != null){

                if(inputLine.startsWith("BEGIN:VEVENT")){
                    course = new Course();
                    i++;
                    Log.i("------------Course","-------------------------------------------------"+i);
                }else if(inputLine.contains("COURSANNULE")){
                    canceled = true;
                }else if(inputLine.startsWith("END:VEVENT")){
                    if(!course.getTitle().isEmpty()){
                        if( !canceled && (course.getDescription().contains(TD) || course.getDescription().contains(PROMOTION)) )  {
                            db.addCourse(course);
                            theCourses.add(course);
                            Log.i("Course-Title","-"+course.getTitle());
                            Log.i("Course-Start","-"+course.getDateStart());
                            Log.i("Course-End","-"+course.getDateEnd());
                            Log.i("Course-description","-"+course.getDescription());
                            Log.i("Course-Type","-"+course.getType());
                            Log.i("Course-Type","COURS AJOUTE");
                        }else
                            Log.i("Course-Type","COURS NON AJOUTE");
                    }

                }else if(inputLine.startsWith("DTSTART:")){
                    try {
                        date_str = inputLine.substring(8,23);
                        Log.i("Course-String to parse ","-"+date_str);
                        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                        date=dateFormat.parse(date_str);
                        course.setDateStart(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    //Log.i("Date Start -- Heure ",(date.getYear()+1900)+"/"+(date.getMonth()+1)+"/"+(date.getDay())+" ** "+date);
                }else if(inputLine.startsWith("DTEND:")){
                    try {
                        date_str = inputLine.substring(6,21);
                        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                        date=dateFormat.parse(date_str);
                        course.setDateEnd(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    //Log.i("Date End -- Heure ",date+" -- ");
                }else if(inputLine.startsWith("DESCRIPTION")){
                    String str0 = inputLine.substring(24,inputLine.length());
                    String str = inputLine.substring(24,inputLine.length());
                    str=str.replace("\\","-");
                    String [] desc = str.split("-n");

                    course.setTitle(desc[0]);
                    course.setDescription(str0);

                    String strType= desc[desc.length-1];
                    strType = (strType.contains("Type")) ?  strType.replace("Type : ","") : "";
                    if(strType.isEmpty()){
                        if(str0.contains("UEO"))
                            strType="UEO";
                    }
                    course.setType(strType);
                }

            }
            //Log.i("Date ------------------------- "+i,"");
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return theCourses;
    }

    public Course getAbonne() {
        return media;
    }
}