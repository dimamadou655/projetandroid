package com.example.projetandroidapp;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Controle extends AppCompatActivity {

    private View view;
    CourseDbHelper db;
    private Calendar cal = Calendar.getInstance();
    private int eventIndex;
    private RelativeLayout mLayout;
    private TextView currentDate;
    ImageView previousDay;
    ImageView nextDay;
    String strNearDate="";
    Date nearDate=null;
    int dateDifference=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controle);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db=new CourseDbHelper(getApplicationContext());
        currentDate = findViewById(R.id.display_current_date);

        mLayout = findViewById(R.id.left_event_column);
        eventIndex = mLayout.getChildCount();
        displayNextDailyEvents();
        //currentDate.setText(displayDateInString(cal.getTime()));
        currentDate.setText(displayDateInString(nearDate));

        previousDay = findViewById(R.id.previous_day);
        nextDay = findViewById(R.id.next_day);
        previousDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previousCalendarDate();
            }
        });
        nextDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextCalendarDate();
            }
        });

    }

    private void previousCalendarDate(){
        mLayout.removeViews(eventIndex-1,mLayout.getChildCount()-eventIndex);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        displayPreviewDailyEvents();
        if(strNearDate.isEmpty()) cal.add(Calendar.DAY_OF_MONTH, (dateDifference+1));
    }
    private void nextCalendarDate(){
        mLayout.removeViews(eventIndex-1,mLayout.getChildCount()-eventIndex);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        displayNextDailyEvents();
        if(strNearDate.isEmpty()) cal.add(Calendar.DAY_OF_MONTH, -(dateDifference+1));
    }

    private String displayDateInString(Date mDate){
        SimpleDateFormat formatter = new SimpleDateFormat("d MMMM, yyyy", Locale.FRENCH);
        return formatter.format(mDate);
    }
    private void displayPreviewDailyEvents(){
        //Log.i("TAG_", "DATE " + new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
        strNearDate = db.getPreviewEvaluation(new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
        dateDifference=0;
        if(!strNearDate.isEmpty()){
            try {
                nearDate = new SimpleDateFormat("yyyy-MM-dd").parse(strNearDate);
                Date ddeb = new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
                long milli = nearDate.getTime() - ddeb.getTime();
                dateDifference = (int) TimeUnit.MILLISECONDS.toDays(milli);
                cal.add(Calendar.DAY_OF_MONTH, dateDifference);
                //Log.i("TAG_", "DATE_DDEB " + strNearDate+" FIN "+new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
                //Log.i("TAG_", "DATE_ADD " + milli+" to Day "+TimeUnit.MILLISECONDS.toDays(milli));
                currentDate.setText(displayDateInString(nearDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Cursor cursor=db.fetchEvaluationByDate(strNearDate);
            cursor.moveToPosition(-1);
            while(cursor.moveToNext()){
                Course course = db.cursorToCourse(cursor);
                Date dateStart = course.getDateStart();
                Date dateEnd = course.getDateEnd();
                String description = course.getDescription();
                int eventBlockHeight = getEventTimeFrame(dateStart, dateEnd);
                //Log.i("db_c", course.getTitle() + " at = " + mLayout.getChildCount() ) ;
                displayEventSection(dateStart, eventBlockHeight, description);
            }
            //Log.i("TAG_", "NO EMPTY " + strNearDate);
        }else{
            //Log.i("TAG_", "EMPTY " + strNearDate);
            nearDate = cal.getTime();
            Toast.makeText(getApplicationContext(),"Plus aucune évaluation",Toast.LENGTH_SHORT).show();
        }
        //Log.i("TAG_", "DATE_AP " + new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
    }

    private void displayNextDailyEvents(){
        strNearDate = db.getNextEvaluation(new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
        dateDifference=0;
        if(!strNearDate.isEmpty()){
            try {
                nearDate = new SimpleDateFormat("yyyy-MM-dd").parse(strNearDate);
                Date ddeb = new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
                long milli = nearDate.getTime() - ddeb.getTime();
                dateDifference = (int) TimeUnit.MILLISECONDS.toDays(milli);
                cal.add(Calendar.DAY_OF_MONTH, dateDifference);
                currentDate.setText(displayDateInString(nearDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Cursor cursor=db.fetchEvaluationByDate(strNearDate);
            cursor.moveToPosition(-1);
            while(cursor.moveToNext()){
                Course course = db.cursorToCourse(cursor);
                Date dateStart = course.getDateStart();
                Date dateEnd = course.getDateEnd();
                String description = course.getDescription();
                int eventBlockHeight = getEventTimeFrame(dateStart, dateEnd);
                displayEventSection(dateStart, eventBlockHeight, description);
            }
        }else{
            nearDate = cal.getTime();
            Toast.makeText(getApplicationContext(),"Plus aucune évaluation",Toast.LENGTH_SHORT).show();
        }
    }

    private int getEventTimeFrame(Date start, Date end){
        long timeDifference = end.getTime() - start.getTime();
        Calendar mCal = Calendar.getInstance();
        mCal.setTimeInMillis(timeDifference);
        long minutes_ = TimeUnit.MILLISECONDS.toMinutes(timeDifference);
        return (int) minutes_;
    }
    private void displayEventSection(Date eventDate, int height, String description){
        SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        String displayValue = timeFormatter.format(eventDate);
        String[]hourMinutes = displayValue.split(":");
        int hours = Integer.parseInt(hourMinutes[0]);
        int minutes = Integer.parseInt(hourMinutes[1]);
        //Log.i("db_", "Hour value " + hours);
        //Log.i("db_", "Minutes value " + minutes);
        //int topViewMargin = ((hours * 60) + ((minutes * 60) / 100))/2;
        int topViewMargin = hours*60 + (minutes-480);
        //Log.i("db_", "Margin top " + topViewMargin);

        createEventView(topViewMargin, height, description);
    }
    private void createEventView(int topMargin, int height, String description){
        TextView mEventView = new TextView(getApplicationContext());
        RelativeLayout.LayoutParams lParam = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lParam.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        lParam.topMargin = topMargin * 2;
        lParam.leftMargin = 0;
        mEventView.setLayoutParams(lParam);
        mEventView.setPadding(10, 0, 10, 0);
        mEventView.setHeight(height * 2);
        mEventView.setGravity(0x11);
        mEventView.setTextColor(Color.parseColor("#ffffff"));
        //mEventView.setText(String.format(description, "%s"));
        mEventView.setText(String.format(description.replace("\\n", "%n")));
        mEventView.setBackgroundColor(Color.parseColor("#3F51B5"));
        mEventView.setBackgroundDrawable(getResources().getDrawable(R.drawable.border));
        mLayout.addView(mEventView,mLayout.getChildCount()-1);
    }

}
